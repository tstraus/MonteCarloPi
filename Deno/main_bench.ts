import { calculatePi, inUnitCircle } from "./main.ts";

Deno.bench(function inUnitCircleBench() {
  inUnitCircle(Math.random(), Math.random());
});

Deno.bench(function piBench() {
  calculatePi(1000000);
});
