export function inUnitCircle(x: number, y: number): boolean {
  return x * x + y * y < 1.0;
}

export function monteCarloPi(reps: number): number {
  let count = 0;

  for (let i = 0; i < reps; i++) {
    if (inUnitCircle(Math.random(), Math.random())) {
      count++;
    }
  }

  return count;
}

export function calculatePi(reps: number): number {
  return monteCarloPi(reps) / reps * 4.0;
}

// Learn more at https://deno.land/manual/examples/module_metadata#concepts
if (import.meta.main) {
  console.log(`pi: ${calculatePi(Number(Deno.args[0]))}`);
}
