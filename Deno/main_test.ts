import {
  assert,
  assertEquals,
} from "https://deno.land/std@0.193.0/testing/asserts.ts";
import { calculatePi, inUnitCircle, monteCarloPi } from "./main.ts";

Deno.test(function unitCircleTest() {
  assertEquals(inUnitCircle(0.1, 3), false);
  assertEquals(inUnitCircle(1, 1), false);
  assertEquals(inUnitCircle(0.7, 0.7), true);
});

Deno.test(function monteCarloTest() {
  assert(calculatePi(1000000) > 3.1);
  assert(calculatePi(1000000) < 3.2);
});
