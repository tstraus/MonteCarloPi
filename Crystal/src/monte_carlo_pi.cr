def inUnitCircle(x, y)
  x*x + y*y < 1.0
end

def monteCarloPi(reps)
  count = 0
  r = Random.new

  reps.times do
    if inUnitCircle(r.rand, r.rand)
      count += 1
    end
  end

  return count
end

reps = ARGV[0].to_u64
count = monteCarloPi(reps)
pi = count / reps.to_f64 * 4.0

puts "pi: #{pi}"
