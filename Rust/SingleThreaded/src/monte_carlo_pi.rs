extern crate rand;

use std::env;
use rand::Rng;

fn main() {
    let args: Vec<String> = env::args().collect();
    let reps: usize = args[1].parse().unwrap();
    let count = monte_carlo_pi(reps);
    let pi = count as f64 / reps as f64 * 4.0;

    println!("pi: {0}", pi);
}

fn in_unit_circle(x: f64, y: f64) -> usize {
    if x*x + y*y <= 1.0 {
        1
    } else {
        0
    }
}

fn monte_carlo_pi(reps: usize) -> usize {
    let mut count = 0;
    let mut rng = rand::thread_rng();

    let group_size = 512;

    let mut x = Vec::new();
    let mut y = Vec::new();
    x.resize(group_size, 0.0);
    y.resize(group_size, 0.0);

    for _ in 0..reps / group_size {
        for i in 0..group_size {
            x[i] = rng.gen::<f64>();
            y[i] = rng.gen::<f64>();
        }

        for i in 0..group_size {
            count += in_unit_circle(x[i], y[i]);
        }
    }

    let extra_reps = reps % group_size;
    for i in 0..extra_reps {
        x[i] = rng.gen::<f64>();
        y[i] = rng.gen::<f64>();
    }

    for i in 0..extra_reps {
        count += in_unit_circle(x[i], y[i]);
    }

    count
}

#[cfg(test)]
#[test]
fn in_unit_circle_test() {
    assert_eq!(in_unit_circle(0.0, 0.0), true);
    assert_eq!(in_unit_circle(0.5, 0.5), true);
    assert_eq!(in_unit_circle(1.0, 0.0), true);
    assert_eq!(in_unit_circle(0.0, 1.0), true);
    assert_eq!(in_unit_circle(1.0, 0.1), false);
    assert_eq!(in_unit_circle(0.1, 1.0), false);
    assert_eq!(in_unit_circle(1.0, 1.0), false);

}
