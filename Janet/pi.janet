(defn in_unit_circle [x y]
  (< (+ (* x x) (* y y)) 1.0))

(defn monte_carlo_pi [reps]
  (math/seedrandom (os/time))
  (var count 0)

  (for rep 0 reps
    (if (in_unit_circle (math/random) (math/random))
      (set count (+ count 1))))

  count)

(defn main [& args]
  (def reps (scan-number (get args 1)))
  (def pi 
    (* (/ (monte_carlo_pi reps) reps) 4.0))
  
  (print "pi: " pi))
