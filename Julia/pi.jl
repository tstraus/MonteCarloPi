reps = size(ARGS)[1] == 1 ? parse(Int64, ARGS[1]) : 1000000000
println("reps: ", reps)

in_unit_circle(x::Float64, y::Float64) = x*x + y*y <= 1.0 ? 1.0 : 0.0

@time begin
x_s = rand(Float64, reps)
y_s = rand(Float64, reps)

pi = mapreduce(in_unit_circle, +, x_s, y_s) / reps * 4.0
end

println("pi: ", pi)
