#!/usr/bin/guile \
-e main -s
!#

(define (in_unit_circle x y)
  (< (+ (* x x) (* y y)) 1.0))

(define (monte_carlo_pi reps)
  (define count 0.0)
  (set! *random-state* (random-state-from-platform))

  (do ((rep 0 (1+ rep))) ((> rep reps))
     (if (in_unit_circle (random 1.0) (random 1.0))
       (set! count (+ count 1.0))))

  count)

(define (main args)
  (define reps (string->number(list-ref args 1)))
  (define pi 
    (* (/ (monte_carlo_pi reps) reps) 4.0))

  (display (string-append "pi: " (number->string pi)))(newline))
